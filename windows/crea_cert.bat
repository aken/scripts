
rem SOFTWARE OPEN-SSL USADO:  Win32OpenSSL_Light-1_1_0f
rem SI NO HAY CERTIFICADO RAIZ, PRIMERO CREARLO:
rem C:\OpenSSL-Win32\bin\openssl genrsa -out CERTIFICADOS\clave_raiz.pem 2048
rem C:\OpenSSL-Win32\bin\openssl req -new -x509 -key CERTIFICADOS\clave_raiz.pem -out CERTIFICADOS\raiz.cer -days 3650 –config C:\OpenSSL-Win32\bin\openssl.cfg

mkdir CERTIFICADOS
mkdir CERTIFICADOS\%1
C:\OpenSSL-Win32\bin\openssl genrsa -out CERTIFICADOS\%1\mi_clave_privada.pem 2048
C:\OpenSSL-Win32\bin\openssl req -new -key CERTIFICADOS\%1\mi_clave_privada.pem -out CERTIFICADOS\%1\micert.csr -config C:\OpenSSL-Win32\bin\openssl.cfg
C:\OpenSSL-Win32\bin\openssl x509 -req -in CERTIFICADOS\%1\micert.csr -CA CERTIFICADOS\raiz.cer -CAkey CERTIFICADOS\clave_raiz.pem -set_serial 12345 -days 3650 -out CERTIFICADOS\%1\micert.pem
C:\OpenSSL-Win32\bin\openssl pkcs12 -export -in CERTIFICADOS\%1\micert.pem -inkey CERTIFICADOS\%1\mi_clave_privada.pem -out CERTIFICADOS\%1\micert.p12
copy CERTIFICADOS\%1\micert.p12 CERTIFICADOS\%1\%1.p12

