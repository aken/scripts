﻿$espacios = Get-WmiObject -Class Win32_LogicalDisk -Filter "DriveType=3" -ComputerName (Get-Content "c:\temp\equipos.txt") | select -property PSComputerName, Name, FreeSpace, Size | % {$_.FreeSpace="{0:n2}" -f ($_.FreeSpace/1GB); $_.Size="{0:n2}" -f ($_.Size/1GB);  $_};
Write-Host $espacios
for($i=0; $i -lt $espacios.Count; $i++) {
    $total = $espacios[$i].Size;
    $libre = $espacios[$i].FreeSpace;
    $porcentaje = [math]::round(([int]$libre * 100) / [int]$total);
    $color = 'GREEN';
    if ([int]$porcentaje -lt 15) {
        $color = 'YELLOW'; }
        elseif ([int]$porcentaje -lt 5) {
            $color = 'RED'; }
    Write-Host $espacios[$i].PSComputerName -NoNewline; Write-Host '  '  -NoNewline; Write-Host $espacios[$i].Name -NoNewline; Write-Host '  '  -NoNewline;Write-Host $porcentaje% -ForegroundColor $color -NoNewline; Write-Host '  '  -NoNewline; Write-Host '(',TOTAL: $total, LIBRE: $libre,')';
    }

<#
class Win32_LogicalDisk : CIM_LogicalDisk
{
  uint16   Access;
  uint16   Availability;
  uint64   BlockSize;
  string   Caption;
  boolean  Compressed;
  uint32   ConfigManagerErrorCode;
  boolean  ConfigManagerUserConfig;
  string   CreationClassName;
  string   Description;
  string   DeviceID;
  uint32   DriveType;
  boolean  ErrorCleared;
  string   ErrorDescription;
  string   ErrorMethodology;
  string   FileSystem;
  uint64   FreeSpace;
  datetime InstallDate;
  uint32   LastErrorCode;
  uint32   MaximumComponentLength;
  uint32   MediaType;
  string   Name;
  uint64   NumberOfBlocks;
  string   PNPDeviceID;
  uint16   PowerManagementCapabilities[];
  boolean  PowerManagementSupported;
  string   ProviderName;
  string   Purpose;
  boolean  QuotasDisabled;
  boolean  QuotasIncomplete;
  boolean  QuotasRebuilding;
  string   Size;
  string   Status;
  uint16   StatusInfo;
  boolean  SupportsDiskQuotas;
  boolean  SupportsFileBasedCompression;
  string   SystemCreationClassName;
  string   SystemName;
  boolean  VolumeDirty;
  string   VolumeName;
  string   VolumeSerialNumber;
};
#>
