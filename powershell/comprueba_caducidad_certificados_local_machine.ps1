$threshold = 30   #Number of days to look for expiring certificates 
$deadline = (Get-Date).AddDays($threshold)   #Set deadline date 

$lm = "Cert:\localmachine\"
$lmDir = Dir Cert:\localmachine\ | select name
foreach ($dir in $lmdir) {
    $path = $lm + $dir.name
    Dir $path | foreach { 
        If ($_.NotAfter -le $deadline) {
            $_ | select @{label="directorio";Expression={$path}}, Issuer, Subject, NotAfter, @{Label="Expires In (Days)";Expression={($_.NotAfter - (Get-Date)).Days}}
        } 
    }
}

