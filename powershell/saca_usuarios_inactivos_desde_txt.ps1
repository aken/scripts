Import-Module ActiveDirectory
Get-Content perfiles.txt | Get-ADUser -ErrorAction SilentlyContinue | Where {$_.Enabled -match "False"} | select samaccountname,DistinguishedName,Enabled  | export-csv usuarios_inactivos_con_perfil.csv -Encoding utf8 -NoTypeInformation -Delimiter ","
