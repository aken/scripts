﻿#Import-Module ActiveDirectory

$admins = Get-ADGroupMember -identity “Administradores” -recursive | select name
foreach ($admin in $admins) {
    Get-ADUser -identity $admin.Name  | select SamAccountName,GivenName,SurName
    }
