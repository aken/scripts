Set-ExecutionPolicy Unrestricted
New-NetFirewallRule -DisplayName ichttpuser -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol TCP -Program "%ProgramFiles% (x86)\inConcert\Agent\ICHttpUser.exe"
New-NetFirewallRule -DisplayName ichttpuser -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol UDP -Program "%ProgramFiles% (x86)\inConcert\Agent\ICHttpUser.exe"
New-NetFirewallRule -DisplayName iciaxphone -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol TCP -Program "%ProgramFiles% (x86)\inConcert\Agent\iciaxphone.exe"
New-NetFirewallRule -DisplayName iciaxphone -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol UDP -Program "%ProgramFiles% (x86)\inConcert\Agent\iciaxphone.exe"
New-NetFirewallRule -DisplayName "SIPPhone Module" -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol TCP -Program "%ProgramFiles% (x86)\inConcert\Agent\SIPPhone.exe"
New-NetFirewallRule -DisplayName "SIPPhone Module" -Action Allow -Authentication NotRequired -Direction Inbound -Enabled True -InterfaceType Any -Protocol UDP -Program "%ProgramFiles% (x86)\inConcert\Agent\SIPPhone.exe"
