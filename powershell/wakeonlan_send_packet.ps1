# Script to wake up a computer. 
# 
# v1.0 - 2012-07-30 Pedro Castro 
# usage: 
#        .\Ligar-Maquina.ps1 -mac 00:1D:92:51:4C:41 
 
param( 
    [string]$mac = '18:60:24:AA:E1:42' #address of the network card (MAC address) 
) 
 
#checks the syntax of MAC address 
if (!($mac -like "*:*:*:*:*:*") -or ($mac -like "*-*-*-*-*-*")){ 
    write-error "mac address not in correct format" 
    break 
} 

write "mac: "$mac

#build magic package http://en.wikipedia.org/wiki/Wake-on-LAN#Magic_packet  
$string=@($mac.split(":""-") | foreach {$_.insert(0,"0x")}) 
$target = [byte[]]($string[0], $string[1], $string[2], $string[3], $string[4], $string[5]) 
# The magic packet is a broadcast frame containing anywhere within its payload 6 bytes of all 255 (FF FF FF FF FF FF in hexadecimal) 
$packet = [byte[]](,0xFF * 102) 
# followed by sixteen repetitions of the target computer's 48-bit MAC address, for a total of 102 bytes. 
6..101 |% { $packet[$_] = $target[($_%6)]} 
     
# .NET framework lib para sockets 
$UDPclient = new-Object System.Net.Sockets.UdpClient 
$UDPclient.Connect(([System.Net.IPAddress]::Broadcast),4000) 
$UDPclient.Send($packet, $packet.Length) | out-null
