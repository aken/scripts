﻿$U = Import-Csv usuarios.txt -Delimiter ' ' -Header 'NOMBRE', 'APELLIDO-1', 'APELLIDO-2'
#$U | Format-Table
$salida = @(foreach ($user in $U) {
    $givenname = $user.NOMBRE
    $surname = $user.'APELLIDO-1' + ' ' + $user.'APELLIDO-2'
    $usuario = $givenname + ' ' + $surname
    Echo $usuario
    Get-aduser -filter {GivenName -eq $givenname -and Surname -like $surname} -Properties * | select SamAccountName,HomeDirectory
    })
Echo $salida > salida.txt
