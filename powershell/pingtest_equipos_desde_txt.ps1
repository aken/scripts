$servidores = Get-Content equipos.txt

foreach ($server in $servidores) {
    Write-Host ">> Pingeando al equipo " $server
	$testping = Test-Connection $server -Quiet
	if ($testping -like "True") {
		Write-Host " +" $server "Si responde"
		"SI " + $server >> resultado_ping.txt
		}
	else {
		Write-Host " -" $server "No responde"
		"NO " + $server >> resultado_ping.txt
		}
    }
