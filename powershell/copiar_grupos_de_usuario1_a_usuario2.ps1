# poner los login entre comillas simples
$userOrigen = 'usrorigen' # LOGIN DEL USUARIO QUE TIENE LOS GRUPOS
$userDestino = 'usrdestino'    # LOGIN DEL USUARIO AL QUE HAY QUE COPIAR LOS GRUPOS
$grupos = Get-ADPrincipalGroupMembership $userOrigen | select distinguishedName

# GUARDO LOS GRUPOS ORIGINALES DEL USUARIO DESTINO POR SI HUBIESE QUE RECUPERARLOS
$fichero = 'Grupos_originales_de_' + $userDestino + '.txt'
Get-ADPrincipalGroupMembership $userDestino | select distinguishedName | Out-File -FilePath $fichero 

foreach ($grupo in $grupos) {
    Write-Host ">> A�adiendo " $userDestino " al grupo " $grupo.distinguishedName
	Add-ADGroupMember -Identity $grupo.distinguishedName -Members $userDestino
	}
