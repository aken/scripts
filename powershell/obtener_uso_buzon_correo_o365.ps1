﻿$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
Import-PSSession $Session -DisableNameChecking

Get-MailboxStatistics usuario@dominio | fl TotalItemSize,ItemCount,IsArchiveMailbox
Get-Mailbox usuario@dominio | fl IssueWarningQuota,ProhibitSendQuota,ProhibitSendReceiveQuota
