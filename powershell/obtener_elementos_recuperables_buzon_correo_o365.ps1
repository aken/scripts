﻿$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection
Import-PSSession $Session -DisableNameChecking

Get-MailboxStatistics usuario@dominio | fl TotalDeletedItemSize,DeletedItemCount,IsArchiveMailbox
Get-Mailbox usuario@dominio | fl RecoverableItemsQuota,RecoverableItemsWarningQuota
