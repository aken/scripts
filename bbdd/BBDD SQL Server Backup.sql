BACKUP DATABASE [Database]
  TO DISK = N'J:\MSSQL11.SQLSRV\MSSQL\Backup\Database.bak'
  WITH NOFORMAT,
       INIT,
       NAME = N'Database-Completa Base de datos Copia de seguridad',
       SKIP,
       NOREWIND,
       NOUNLOAD,
       STATS = 10;

DECLARE @backupSetId AS INT;

SELECT @backupSetId = position
  FROM msdb..backupset
  WHERE database_name=N'Database' AND
        backup_set_id=(SELECT MAX(backup_set_id)
                         FROM msdb..backupset
                         WHERE database_name=N'Database' );

IF @backupSetId IS NULL BEGIN
  RAISERROR(N'Error de comprobación. No se encuentra la información de copia de seguridad para la base de datos ''Database''.', 16, 1)
END

RESTORE VERIFYONLY
  FROM DISK = N'J:\MSSQL11.SQLSRV\MSSQL\Backup\Database.bak'
  WITH FILE = @backupSetId,
       NOUNLOAD,
       NOREWIND;
