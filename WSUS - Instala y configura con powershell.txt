
#Primero nos conectamos al equipo Core donde vamos a instalar el WSUS
Enter-PSSession -ComputerName WSUSSRV01

#Miramos que características tenemos que instalar
Get-WindowsFeature UpdateServices*

#Instalamos todos menos SQL
Install-WindowsFeature UpdateServices,UpdateServices-WidDB,UpdateServices-Services

#Comprobamos que se han instalado todo
Get-WindowsFeature UpdateServices*

#Inicializamos el disco y preparamos apra recibir los datos de las actualizaciones.
Get-Disk
Initialize-Disk -Number 1 -PartitionStyle MBR
New-Partition -DiskNumber 1 -UseMaximumSize
Get-Partition -PartitionNumber 1 | Format-Volume -FileSystem NTFS -NewFileSystemLabel «Data» -Confirm: $false
Set-Partition -DiskNumber 1 -NewDriveLetter E

#Llevamos  a cabo la postinstalacion y le indicamos la ruta donde guardar las descargas
cd ‘C:\Program Files\Update Services\Tools’
WsusUtil.exe  postinstall /?
wsusutil.exe postinstall CONTENT_DIR=E:\WSUS

#Creamos las variables necesarias para configurar el WSUS
$wsus=Get-WsusServer 

#Ya que solo tenemos un servidor WSUS. Guardamos la configuración en una variable
$wsusconfig=$wsus.GetConfiguration() 


#Configuramos el servidor como Upstream.
Set-WsusServerSynchronization -SyncFromMU

#Indicamos que solo queremos descargar actualizaciones y parches en inglés
$wsusconfig.AllUpdateLanguagesDssEnabled =$false 
$wsusconfig.SetEnabledUpdateLanguages(«en») 
$wsusconfig.Save()

#Descargamos las bases de actualizaciones y parches
$subscription=$wsus.GetSubscription()
$subscription.StartSynchronizationForCategoryOnly()
While ($subscription.GetSynchronizationStatus() -ne ‘NotProcessing’){
    Write-Host «.» -NoNewline
    Start-Sleep -Seconds 20
}
Write-Host  «Sync is done»

#Seleccionamos los productos de los que queremos descargar actualizaciones y parches
Get-WsusProduct | Where-Object{
    $_.Product.Title -in(
      ‘Windows Server 2016’,
      ‘Windows 10’
      )
    }|Set-WsusProduct

#Clasificacion de actualizaciones que vamos a instalar. En nuestro caso solo Security y Critical.
Get-WsusClassification | Where-Object {
    $_.Classification.Title -in (
		#’Update Rollups’,
        ‘Security Updates’,
        ‘Critical Updates’
		#’Service Packs’,
		#’Updates’
		)
    } | Set-WsusClassification

#Vamos a programar la sincronizacion, para decirle cuando se va a ejecutar
$subscription.SynchronizeAutomatically=$true
$subscription.SynchronizeAutomaticallyTimeOfDay=(New-TimeSpan -Hours 0)
$subscription.NumberOfSynchronizationsPerDay=1
$subscription.Save()
$subscription.StartSynchronization()

#Aprobamos las actualizaciones y parches descargados
Get-WsusUpdate -Classification All -Approval Unapproved -Status FailedOrNeeded | Approve-WsusUpdate -Action -TargetGroupName “Unassigned Computers”

#Ver si tenemos equipos registrados en el WSUS
Get-WsusComputer
