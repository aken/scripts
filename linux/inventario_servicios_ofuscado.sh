#!/bin/bash
# GPL ETD

LOG_PATH="/tmp/inventario_servicios"
LOG_PATH_CSV="$LOG_PATH/csv"
LOG_PATH_TMP="$LOG_PATH/tmp"

LOG="resultadoInventarioServicios.txt"
LOG_RESULTADOS="$LOG_PATH/$LOG"
LISTA_IPs="$LOG_PATH_TMP/listaIPs"
LISTA_IPs_ANSIBLE="$LOG_PATH_TMP/listaIPsAnsible"
LOG_PHP_CSV="$LOG_PATH_CSV/listadoServidoresPhp.csv"
LOG_NGINX_CSV="$LOG_PATH_CSV/listadoServidoresNginx.csv"
LOG_APACHE_CSV="$LOG_PATH_CSV/listadoServidoresApache.csv"
LOG_TOMCAT_CSV="$LOG_PATH_CSV/listadoServidoresTomcat.csv"
LOG_MARIADB_CSV="$LOG_PATH_CSV/listadoServidoresMariaDB.csv"
LOG_POSTGRESQL_CSV="$LOG_PATH_CSV/listadoServidoresPostgreSQL.csv"
LOG_WILDFLY_CSV="$LOG_PATH_CSV/listadoServidoresWildfly.csv"
LOG_DRUPAL_CSV="$LOG_PATH_CSV/listadoServidoresDrupal.csv"
LOG_WORDPRESS_CSV="$LOG_PATH_CSV/listadoServidoresWordpress.csv"
LOGS_MONTAJES_CSV="$LOG_PATH_CSV/listadoServidoresConMontajesCifsNfs.csv"
#LOG_FILE SE DEFINE EN LA FUNCION DE INICIO SIENDO LA UNION DE HOSTNAME E IP DE CADA SERVIDOR DENTRO DEL DIRECTORIO tmp

cd $LOG_PATH || mkdir -p $LOG_PATH 2>&1
mkdir -p $LOG_PATH_TMP 2>&1
mkdir -p $LOG_PATH_CSV 2>&1
mkdir -p $LOG_PATH/antiguos 2>&1

# vaciamos las carpetas tmp y csv de los logs existentes, haciendo antes un backup
NOW=$(date +%Y-%m-%d)
tar -zcvf $LOG_PATH/antiguos/$NOW-tmp.tgz $LOG_PATH_TMP
tar -zcvf $LOG_PATH/antiguos/$NOW-csv.tgz $LOG_PATH_CSV
rm $LOG_PATH_TMP/*.log
rm $LOG_PATH_CSV/*.csv
mv $LOG_RESULTADOS $LOG_PATH/antiguos/$LOG.$NOW

#inicializamos el fichero de log y los CSV
true > $LOG_RESULTADOS
echo "HOSTNAME#IP#Version#php-fpm#Estado#Alternatives#Configuracion#root_path#fastcgi" > $LOG_PHP_CSV
echo "HOSTNAME#IP#Version#Estado#Configuracion#Servicio#root_path" > $LOG_NGINX_CSV
echo "HOSTNAME#IP#Version#Carpeta" > $LOG_APACHE_CSV
echo "HOSTNAME#IP#Version#Carpeta#javaHome#javaVersion#enUso" > $LOG_TOMCAT_CSV
echo "HOSTNAME#IP#Version#Estado" > $LOG_MARIADB_CSV
echo "HOSTNAME#IP#Version#Estado" > $LOG_POSTGRESQL_CSV
echo "HOSTNAME#IP#Servicio#Estado#Version#Path#connectionUrl" > $LOG_WILDFLY_CSV
echo "HOSTNAME#IP#Configuracion#Server#serverRoot#Version_Drupal#Version_Drush" > $LOG_DRUPAL_CSV
echo "HOSTNAME#IP#path" > $LOG_WORDPRESS_CSV
echo "HOSTNAME#IP#Servidor de ficheros#Punto de montaje" > $LOGS_MONTAJES_CSV
echo "HOSTNAME#IP#Tipo#Export#Tamaño#Usado#Disponible#%Uso#Montado en" > $LOGS_MONTAJES_CSV

## LA FUNCION INICIO SE ENCUENTRA AL FINAL, DESPUES DE LAS FUNCIONES

##############################################################################
#     FUNCIONES
##############################################################################


generaListaIPs() {
    # generamos listado de ips en base a ansible
    echo "Buscando IPs CentOS"
    ansible centos -m ping > "$LISTA_IPs_ANSIBLE"
    echo "Buscando IPs Ubuntu"
    ansible ubuntu -m ping >> "$LISTA_IPs_ANSIBLE"
    echo "Buscando IPs RedHat"
    ansible redhat -m ping >> "$LISTA_IPs_ANSIBLE"
    echo "Buscando IPs Oracle"
    ansible oracle -m ping >> "$LISTA_IPs_ANSIBLE"
    echo "Buscando IPs SuSe"
    ansible suse -m ping >> "$LISTA_IPs_ANSIBLE"

    # generamos el listado solo con las ips desechando el resto de información que da ansible
    echo "Creamos el fichero con IPs"
    sudo cat "$LISTA_IPs_ANSIBLE" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" | sort | uniq > "$LISTA_IPs"
}


# Apache (apache2)
compruebaApache2() {
    echo "=== APACHE ===" >> "$LOG_FILE"
    apache=$(ssh globaluser@"$IP" "dpkg -l | grep apache2 | grep ii | sort | head -1 | awk '{ print \$2 }'")
    if [ "$apache" = "apache2" ]; then
        apache_version=$(ssh globaluser@"$IP" "/usr/sbin/apache2 -v | grep "\"Server version"\" | awk -F ":" '{ print \$2 }' | sed 's/\ //g'")
        apache_carpeta=$(ssh globaluser@"$IP" "/usr/sbin/apache2 -V | grep HTTPD_ROOT | awk '{ print \$2 }' | awk -F "=" '{ print \$2 }' | sed 's/\"//g'")
        {
            echo "  Apache APACHE2"
            echo "  Version: $apache_version"
            echo "  Carpeta: $apache_carpeta"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$apache_version#$apache_carpeta" >> "$LOG_APACHE_CSV"
    fi
}
# Apache (httpd)
compruebaApache_httpd() {
    echo "=== APACHE ===" >> "$LOG_FILE"
    apache=$(ssh globaluser@"$IP" "rpm -qa | grep "^httpd-[0-9]" | sort | awk  -F"-" '{ print \$1 }'")
    if [ "$apache" = "httpd" ]; then
        apache_version=$(ssh globaluser@"$IP" "/usr/sbin/httpd -v | grep "\"Server version"\" | awk -F ":" '{ print \$2 }' | sed 's/\ //g'")
        apache_carpeta=$(ssh globaluser@"$IP" "/usr/sbin/httpd -V | grep HTTPD_ROOT | awk '{ print \$2 }' | awk -F "=" '{ print \$2 }' | sed 's/\"//g'")
        {
            echo "  Apache HTTPD"
            echo "  Version: $apache_version"
            echo "  Carpeta: $apache_carpeta"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$apache_version#$apache_carpeta" >> "$LOG_APACHE_CSV"
    fi
}


# Apache Tomcat
compruebaTomcat () {
    echo "=== APACHE TOMCAT ===" >> "$LOG_FILE"
    # primero buscamos instalaciones estándar y luego instalaciones independientes usando locate
    tomcat=$(ssh globaluser@"$IP" "/sbin/tomcat version | head -1 | awk '{ print \$4 }' | awk -F '/' '{ print \$1 }'")
    if [ "$tomcat" = "Tomcat" ]; then
        tomcatVersion=$(ssh globaluser@"$IP" "/sbin/tomcat version | head -1")
        {
            echo "  Tomcat Distro Release"
            echo "  Version: $tomcatVersion"
            echo "  Carpeta: /sbin/tomcat"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$tomcatVersion#'/sbin/tomcat'###" >> "$LOG_TOMCAT_CSV"
    fi
    tomcat_localizaciones=$(ssh globaluser@"$IP" "/usr/bin/locate version.sh | grep tomcat")
    for tomcatVersionFile in $tomcat_localizaciones
    do
        tomcatVersion=$(ssh globaluser@"$IP" "$tomcatVersionFile | grep "\"Server version"\"")
        carpetaTomcat=$(echo "$tomcatVersionFile" | awk -F "/" '{ print "/"$2"/"$3"/"$4 }')
        javaHome=$(echo "$tomcatVersionFile" | awk -F "/" '{ print "/"$2"/"$3 }')
        javaVersion=$(ssh globaluser@"$IP" "ls -l $javaHome/jdk | awk '{print \$NF}'")
        versionTomcat=$(echo "$tomcatVersion" | awk -F ":" '{ print $2 }')
        homeTomcat=$(echo "$javaHome" | awk -F "/" '{ print $3 }')
        enUso=$(ssh globaluser@"$IP" "grep $homeTomcat /etc/tomcats")
        if [ "$enUso" = "$homeTomcat" ]; then
            enUso="Si"
        else
            enUso="No"
        fi
        {
            echo "  Tomcat Standalone Release"
            echo "  Version: $versionTomcat"
            echo "  Carpeta: $carpetaTomcat"
            echo "     Java: $javaVersion"
            echo "Java home: $javaHome"
            echo "   En Uso: $enUso"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$versionTomcat#$carpetaTomcat#$javaHome#$javaVersion#$enUso" >> "$LOG_TOMCAT_CSV"
    done
}


# NGINX
compruebaNginx() {
    echo "=== NGINX ===" >> "$LOG_FILE"
    existeNGINX=$(ssh globaluser@"$IP" "systemctl list-units | grep nginx")
    if [[ "$existeNGINX" == *"nginx.service"* ]]; then
        estadoServicioNginx=$(ssh globaluser@"$IP" "systemctl | grep -w nginx.service | awk '{ print \$2, \$3, \$4 }'")
        configuracionesNginx=$(ssh globaluser@"$IP" "ls -la /etc/nginx/conf.d/ | grep -E '\.conf$' | awk '{ print \$9 }'")
        confs=$(echo "$configuracionesNginx" | sed 's/ /, /g' | wc -l)
        versionNGINX=$(ssh globaluser@"$IP" "/usr/sbin/nginx -v 2>&1 | awk -F ":" '{ print \$2 }' | sed 's/ //'")
        {
            echo "  Version Nginx: $versionNGINX"
            echo "  Estado del servicio: $estadoServicioNginx"
            echo "  Configuraciones:   $confs"
            echo "  Datos de las configuraciones encontradas:"
        } >> "$LOG_FILE"
        # Voy a mirar en todos los .conf de nginx el server name y la ruta de root
        configsNginx=$(ssh globaluser@"$IP" "ls /etc/nginx/conf.d/*.conf")
        for config in $configsNginx
        do
            server_name=$(ssh globaluser@"$IP" "grep 'server_name' $config | head -n 1 | awk '{print \$NF}' | sed -e 's/.$//'")
            root_path=$(ssh globaluser@"$IP" "grep 'root' $config  | grep -v '#root' | head -n 1 | awk '{print \$NF}' | sed -e 's/.$//'")
            {
                echo "  Fichero de configuración: $config"
                echo "               Server Name: $server_name"
                echo "               Server Root: $root_path"
            } >> "$LOG_FILE"
            echo "$HOSTNAME#$IP#$versionNGINX#$estadoServicioNginx#$config#$server_name#$root_path" >> "$LOG_NGINX_CSV"
        done
    fi
}


# PHP
compruebaPHP() {
    echo "=== PHP ===" >> "$LOG_FILE"
    existePhp=$(ssh globaluser@"$IP" "systemctl list-units | grep php | grep .service")
    if [[ "$existePhp" == *"-fpm.service"* ]]; then
        phpfpm=$(ssh globaluser@"$IP" "systemctl list-units | grep php | grep .service | awk '{ print \$1 }'")
        versionPhp=$(ssh globaluser@"$IP" "php -v | head -n1")
        estadoPhp=$(ssh globaluser@"$IP" "systemctl list-units | grep php | grep .service | awk '{ print \$2, \$3, \$4 }'")
        alternativesPhp=$(ssh globaluser@"$IP" "update-alternatives --get-selections | grep 'php ' | awk -F' ' '{ print \$1\" \"\$2\" \"\$3 }'")
        {
            echo "  Version Php: $versionPhp"
            echo "  Estado $phpfpm: $estadoPhp"
            echo "  Alternatives: $alternativesPhp"
        } >> "$LOG_FILE"
        existeNGINX=$(ssh globaluser@"$IP" "systemctl list-units | grep nginx")
        if [[ "$existeNGINX" == *"nginx.service"* ]]; then
            configs=$(ssh globaluser@"$IP" "ls /etc/nginx/conf.d/*.conf")
            echo "  configuracion nginx  :  directorio root  :  fastcgi_pass" >> "$LOG_FILE"
            for config in $configs
            do
                rootPath=$(ssh globaluser@"$IP" "grep 'root' $config  | grep -v '#root' | head -n 1 | awk '{print \$NF}' | sed -e 's/.$//'")
                fastcgi=$(ssh globaluser@"$IP" "grep 'fastcgi_pass' $config | grep -v \"#\"")
                # con el awk quitamos los espacios en blanco del principio
                fastcgiPass=$(echo "$fastcgi" | awk '{sub(/^[ \t]+/, ""); print }')
                if [ "$fastcgiPass" != "" ]; then
                    echo "  $config  :  $rootPath  :  $fastcgiPass  " >> "$LOG_FILE"
                    echo "$HOSTNAME#$IP#$versionPhp#$phpfpm#$estadoPhp#$alternativesPhp#$config#$rootPath#$fastcgiPass" >> "$LOG_PHP_CSV"
                fi
            done
        else
            echo "$HOSTNAME#$IP#$versionPhp#$phpfpm#$estadoPhp#$alternativesPhp###" >> "$LOG_PHP_CSV"
        fi
    fi
}


# MariaDB o mySQL
compruebaMariaDb_mySQL() {
    echo "=== MARIADB o MYSQL ===" >> "$LOG_FILE"
    existeMariadb=$(ssh globaluser@"$IP" "systemctl list-units | grep mariadb")
    existeMysql=$(ssh globaluser@"$IP" "systemctl list-units | grep mysqld")
    if [[ "$existeMariadb" == *"mariadb.service"* ]] || [[ "$existeMysql" == *"mysqld.service"* ]]; then
        tipoBBDD=$(ssh globaluser@"$IP" "mysql -V")
    else
        return
    fi
    if [[ "$tipoBBDD" == *"MariaDB"* ]]; then
        estadoServicioBBDD=$(ssh globaluser@"$IP" "systemctl list-units | grep mariadb.service | awk '{ print \$2, \$3, \$4 }'")
    else
        estadoServicioBBDD=$(ssh globaluser@"$IP" "systemctl list-units | grep mysqld.service | awk '{ print \$2, \$3, \$4 }'")
    fi
    {
        echo "  MariaDB" >> "$LOG_FILE"
        echo "  Version: $tipoBBDD"
        echo "  Estado del servicio: $estadoServicioBBDD"
    } >> "$LOG_FILE"
    echo "$HOSTNAME#$IP#$tipoBBDD#$estadoServicioBBDD" >> "$LOG_MARIADB_CSV"
}


# PostgreSQL
compruebaPostgreSQL() {
    echo "=== POSTGRESQL ===" >> "$LOG_FILE"
    existePostgresql=$(ssh globaluser@"$IP" "systemctl list-units | grep postgresql")
    if [[ "$existePostgresql" == *"postgresql.service"* ]]; then
        versionPSQL=$(ssh globaluser@"$IP" "psql -V")
        estadoServicioPSQL=$(ssh globaluser@"$IP" "systemctl | grep postgresql.service | awk '{ print \$2, \$3, \$4 }'")
        {
            echo "  Version Postgresql: $versionPSQL"
            echo "  Estado del servicio: $estadoServicioPSQL"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$versionPSQL#$estadoServicioPSQL" >> "$LOG_POSTGRESQL_CSV"
    fi
}


# Fail2ban
compruebaFail2ban() {
    echo "=== FAIL2BAN ===" >> "$LOG_FILE"
    existeFail2ban=$(ssh globaluser@"$IP" "systemctl list-units | grep fail2ban")
    if [[ "$existeFail2ban" == *"fail2ban.service"* ]]; then
        versionFail2ban=$(ssh globaluser@"$IP" "fail2ban-client --version")
        estadoServicioFail2ban=$(ssh globaluser@"$IP" "systemctl | grep fail2ban.service | awk '{ print \$2, \$3, \$4 }'")
        jaulasFail2ban=$(ssh globaluser@"$IP" "sudo fail2ban-client status | grep 'Jail list' | cut -d':' -f2-")
        {
            echo "  Version Fail2ban: $versionFail2ban"
            echo "  Estado del servicio: $estadoServicioFail2ban"
            echo "  Jaulas configuradas: $jaulasFail2ban"
        } >> "$LOG_FILE"
    fi
}


# Wildfly
compruebaWildfly() {
    echo "=== WILDFLY ===" >> "$LOG_FILE"
    existeWildfly=$(ssh globaluser@"$IP" "systemctl list-units | grep wildfly")
    if [[ "$existeWildfly" == *"wildfly"* ]]; then
        echo "  Wildfly" >> "$LOG_FILE"
        cantidadServiciosWildfly=$(ssh globaluser@"$IP" "systemctl list-units | grep wildfly | wc -l")
        serviciosWildfly=$(ssh globaluser@"$IP" "systemctl list-units | grep wildfly | awk -F '.' '{ print \$1 }'")
        echo "  Número de servicios Wildfly activos: $cantidadServiciosWildfly" >> "$LOG_FILE"
        for servicio in $serviciosWildfly
        do
            estadoServicioWildfly=$(ssh globaluser@"$IP" "systemctl | grep $servicio.service | awk '{ print \$2, \$3, \$4 }'")
            echo "  Estado de $servicio: $estadoServicioWildfly" >> "$LOG_FILE"
            versionWildfly=$(ssh globaluser@$IP "sudo grep "WFLYSRV0049" /opt/$servicio/standalone/log/server.* | awk '{ print \$10 \$11 \$12 \$13 \$14 \$15 }' | sort | tail -1")
            echo "  Version de $servicio: $versionWildfly" >> "$LOG_FILE"
            carpetaWildfly=$(ssh globaluser@$IP "sudo /opt/$servicio/bin/standalone.sh --version | grep "JBOSS_HOME" | awk '{ print \$2 }'")
            echo "  Carpeta de $servicio: $carpetaWildfly" >> "$LOG_FILE"
            connectionUrl=$(ssh globaluser@"$IP" "sudo grep '<connection-url>jdbc:oracle:thin:@' /opt/$servicio/standalone/configuration/standalone.xml | awk -F ':' '{ print \$5 }' | awk -F '/' '{print \$2}' | sed -e 's/.$//' | tr '\n' ','")
            echo "  Connections URL de $servicio: $connectionUrl" >> "$LOG_FILE"
            echo "$HOSTNAME#$IP#$servicio#$estadoServicioWildfly#$versionWildfly#$carpetaWildfly#$connectionUrl" >> "$LOG_WILDFLY_CSV"
        done
    fi
}


# Drupal
compruebaDrupal() {
    echo "=== DRUPAL / DRUSH ===" >> "$LOG_FILE"
    # Lo primero es ver si existe drupal instalado, si no existe no devolvera nada y nos marcharemos
    existeDrupal=$(ssh globaluser@"$IP" "drush status && echo valido || echo error")
    if [[ "$existeDrupal" == *"error"* ]]; then
        return
    fi
    echo "  Drupal" >> "$LOG_FILE"
    # Voy a mirar en todos los .conf de nginx la ruta root y ejecutar 'drush status' para ver si tiene drupal y sacar la info
    confsNginx=$(ssh globaluser@"$IP" "ls /etc/nginx/conf.d/*.conf")
    for conf in $confsNginx
    do
        serverName=$(ssh globaluser@"$IP" "grep 'server_name' $conf | head -n 1 | awk '{print \$NF}' | sed -e 's/.$//'")
        serverRoot=$(ssh globaluser@"$IP" "grep 'root' $conf | head -n 1 | awk '{print \$NF}' | sed -e 's/.$//'")
        infoDrupal=$(ssh globaluser@"$IP" "sudo su -c 'cd $serverRoot; drush status'")
        versionDrupal=$(echo "$infoDrupal" | grep 'Drupal version' | awk -F ':' '{print $2}')
        versionDrush=$(echo "$infoDrupal" | grep 'Drush version' | awk -F ':' '{print $2}')
        {
            echo "  Configuración nginx: $conf"
            echo "          Server Name: $serverName"
            echo "          Server Root: $serverRoot"
            echo "       Version Drupal: $versionDrupal"
            echo "        Version Drush: $versionDrush"
        } >> "$LOG_FILE"
        echo "$HOSTNAME#$IP#$conf#$serverName#$serverRoot#$versionDrupal#$versionDrush" >> "$LOG_DRUPAL_CSV"
    done
}


# Wordpress
compruebaWordpress() {
    echo "=== WORDPRESS ===" >> "$LOG_FILE"
    existeWordpress=$(ssh globaluser@"$IP" "sudo find /usr/share/nginx -maxdepth 6 -name wp-admin")
    if [[ "$existeWordpress" == "" ]]; then
        return
    fi
    echo "  Wordpress" >> "$LOG_FILE"
    pathWp=$(echo "$existeWordpress" | awk '{print $1}')
    echo "  Path: $pathWp" >> "$LOG_FILE"
    for path in $pathWp
    do
        echo "$HOSTNAME#$IP#$path" >> "$LOG_WORDPRESS_CSV"
    done
}


# Puntos de montaje CIFS / NFS
compruebaMontajesNfsCifs() {
    echo "=== CIFS/NFS ===" >> "$LOG_FILE"
    cifs_fs=$(ssh globaluser@"$IP" "sudo df -t cifs -h")
    nfs_fs=$(ssh globaluser@"$IP" "sudo df -t nfs -h")
    nfs4_fs=$(ssh globaluser@"$IP" "sudo df -t nfs4 -h")
    cifs=$(echo "$cifs_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
    nfs=$(echo "$nfs_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
    nfs4=$(echo "$nfs4_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
    if [[ "$cifs" != "" || "$nfs" != "" || "$nfs4" != "" ]]; then
        {
        echo "  cifs/nfs"
        echo "$cifs_fs"
        echo "$nfs_fs"
        echo "$nfs4_fs"
        } >> "$LOG_FILE"
    fi
    if [[ "$cifs" != "" ]]; then
        for fs in $cifs
        do
            echo "$HOSTNAME $IP cif $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }' >> "$LOGS_MONTAJES_CSV"
        done
    fi
    if [[ "$nfs" != "" ]]; then
        #nfsVersion=$(ssh globaluser@"$IP" "nfsstat -c | grep 'Client nfs' | awk -F ":" '{ print \$1 }' | awk '{ print \$3 }' | paste -s -d_") # el paste es por si hay varias versiones en varias lineas las concatene
        for fs in $nfs
        do
            echo "$HOSTNAME $IP nfs_v3 $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }' >> "$LOGS_MONTAJES_CSV"
        done
    fi
    if [[ "$nfs4" != "" ]]; then
        for fs in $nfs4
        do
            echo "$HOSTNAME $IP nfs_v4 $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }' >> "$LOGS_MONTAJES_CSV"
        done
    fi
}


compruebaServicios() {
    # Apache se comprueba de forma independiente ya que en unos es apache2 y en otros es httpd
    if [[ "$queEscaneo" = "2" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Apache Tomcat..."
        compruebaTomcat
    fi
    if [[ "$queEscaneo" = "3" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Nginx..."
        compruebaNginx
    fi
    if [[ "$queEscaneo" = "4" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando PHP..."
        compruebaPHP
    fi
    if [[ "$queEscaneo" = "5" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Mariadb y/o Mysql..."
        compruebaMariaDb_mySQL
    fi
    if [[ "$queEscaneo" = "6" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Postgresql..."
        compruebaPostgreSQL
    fi
    if [[ "$queEscaneo" = "7" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Wildfly..."
        compruebaWildfly
    fi
    if [[ "$queEscaneo" = "8" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Drupal / Drush..."
        compruebaDrupal
    fi
    if [[ "$queEscaneo" = "9" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Wordpress..."
        compruebaWordpress
    fi
    if [[ "$queEscaneo" = "A" || "$queEscaneo" = "a" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Fail2ban..."
        compruebaFail2ban
    fi
    if [[ "$queEscaneo" = "B" || "$queEscaneo" = "b" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando puntos de montaje cifs/nfs..."
        compruebaMontajesNfsCifs
    fi
}


infoUbuntu() {
    if [[ "$queEscaneo" = "1" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Apache2..."
        compruebaApache2
    fi
    compruebaServicios
}

infoCentOS() {
    if [[ "$queEscaneo" = "1" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Apache httpd..."
        compruebaApache_httpd
    fi
    compruebaServicios
}

infoRedHat() {
    if [[ "$queEscaneo" = "1" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Apache httpd..."
        compruebaApache_httpd
    fi
    compruebaServicios
}

infoOracle() {
    if [[ "$queEscaneo" = "1" || "$queEscaneo" = "0" ]]; then
        echo "Comprobando Apache httpd..."
        compruebaApache_httpd
    fi
    compruebaServicios
}

infoSuse() {
    true
}


compruebaSSH() {
    retorno_ssh=$(ssh -o BatchMode=yes -o ConnectTimeout=5 globaluser@"$IP" "cat /etc/lsb-release 2>&1;cat /etc/oracle-release 2>&1;cat /etc/centos-release 2>&1;cat /etc/redhat-release 2>&1;cat /etc/issue 2>&1")
    return $?
}

sacaVersiones() {
    # Ahora sacamos las versiones de los sistemas operativos y dependiendo de que sea sacamos el resto de información
    case $retorno_ssh in
        *"Ubuntu "*)
            RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            # el if -z comprueba si la variable está vacia
            if [ -z "$RELEASE" ]; then
                RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            fi
            SERVER="Ubuntu $RELEASE"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
            infoUbuntu
        ;;
        *"CentOS "*)
            RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            if [ -z "$RELEASE" ]; then
                RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            fi
            SERVER="CentOS $RELEASE"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
            infoCentOS
        ;;
        *"Red Hat "*)
            RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            if [ -z "$RELEASE" ]; then
                RELEASE=$(echo "$retorno_ssh" | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}" | uniq)
            fi
            SERVER="Red Hat $RELEASE"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
            infoRedHat
        ;;
        *"Oracle "*)
            SERVER="Oracle"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
            infoOracle
        ;;
        *"SUSE "*)
            RELEASE=$(ssh globaluser@"$IP" "cat /etc/SuSE-release | grep SUSE")
            SERVER="Suse $RELEASE"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
            infoSuse
        ;;
        *)
            SERVER="DESCONOCIDO"
            echo "Sistema Operativo:  $SERVER"
            echo "SISTEMA OPERATIVO:  $SERVER" >> "$LOG_FILE"
        ;;
    esac
}


inicio() {
    # Iniciamos el recorrido por los servers en búsqueda de los diferentes servicios
    for IP in $(cat "$LISTA_IPs")
    do
        #HOSTNAME=$(dig -x "$IP" | grep -a1 "ANSWER SECTION:" | grep "PTR" | awk '{print $NF}')
        HOSTNAME=$(ssh "$IP" "hostname -f")
        echo -e "\nComprobando conexión SSH con $HOSTNAME ($IP)"
        if compruebaSSH
        then
            LOG_FILE="$LOG_PATH_TMP/$HOSTNAME-$IP.log"
            {
            echo "************************************"
            echo " Conexión establecida..."
            echo " Servidor: $HOSTNAME ($IP)"
            echo "************************************"
            } > "$LOG_FILE"
            sacaVersiones
            cat "$LOG_FILE" >> "$LOG_RESULTADOS"
        else
            echo "Conexión ERRONEA."
            echo "NO hay conexión con $HOSTNAME ($IP)" >> $LOG_PATH_TMP"/conexion_ssh_fallida.err"
        fi
    done
}


####################################################################################################
#     MODULO MAIN
####################################################################################################
clear
echo ""
echo "Inventario de servicios"
echo ""
if [[ $1 = "ALL" || $1 = "TODOS" || $1 = "all" || $1 = "todos" || $1 = "0" ]]; then
    queEscaneo="0"
    generaListaIPs
    inicio
else
    echo "¿Deseas generar la lista de IPs de nuevo? (SI/NO)"
    read -r respuesta
    if [[ "$respuesta" = "SI" || "$respuesta" = "si" || "$respuesta" = "Si" || "$respuesta" = "S" || "$respuesta" = "s" ]]; then
        generaListaIPs
    fi
    echo ""
    echo "Selecciona un servicio específico o todos"
    echo "-------------------------------------------"
    echo "1) Apache"
    echo "2) Apache Tomcat"
    echo "3) Nginx"
    echo "4) Php"
    echo "5) mariaDB/ mySQL"
    echo "6) postgreSQL"
    echo "7) Wildfly"
    echo "8) Drupal"
    echo "9) Wordpress"
    echo "A) Fail2ban"
    echo "B) Puntos de montaje CIFS/NFS"
    echo ""
    echo "0) TODOS LOS SERVICIOS"
    echo "X) SALIR - EXIT"
    echo ""
    echo "¿Que opción quieres?"
    read -r queEscaneo
    case ${queEscaneo} in
        1|2|3|4|5|6|7|8|9|0|A|a|B|b)
        inicio
        ;;
        X|x)
        exit
        ;;
        *)
        echo ""
        echo "OPCION INVALIDA, VUELVE A INTENTARLO"
        echo""
        ;;
    esac
fi
