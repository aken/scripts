#!/bin/bash

#Si la web es drupal ir al directorio de la web y vaciar cache:
#echo "Borramos cache antes del backup"
#cd /usr/share/nginx/[WEB]/project/docroot
#drush cr

DB_USER="user"
DB_PASSWD="password"
DB_DATABASE="database"
BACKUP_PATH="/var/data/mariadb_backup"
BACKUP_FILE=$DB_DATABASE"-"$(date +%Y-%m-%d-%H-%M)".sql"
LOGFILE=$BACKUP_PATH"/"$DB_DATABASE"_backup.log"
SERVIDOR=$(hostname -f)
DIASMANTENER=15

now=$(date +"%d-%m-%Y")

true > $LOGFILE

echo "=================================================" >> $LOGFILE
echo "Servidor: $SERVIDOR" >> $LOGFILE
echo "BACKUP BBDD $DB_DATABASE - Realizado el $now" >> $LOGFILE
echo "BORRAMOS LOS BACKUPS QUE TIENEN MAS DE $DIASMANTENER DIAS DE ANTIGUEDAD" >> $LOGFILE
find  $BACKUP_PATH/ -type f -name $DB_DATABASE"*" -mtime +$DIASMANTENER -delete
echo "CREANDO COPIA DE SEGURIDAD CON mysqldump" >> $LOGFILE
mysqldump -u $DB_USER -p$DB_PASSWD --single-transaction --quick $DB_DATABASE > $BACKUP_PATH/$BACKUP_FILE  2>>$LOGFILE
if [ "$?" -eq 0 ]
then
    echo "Volcado de la base de datos $DB_DATABASE correcto. Comprimiendo con gzip." >> $LOGFILE
    gzip -9 $BACKUP_PATH/$BACKUP_FILE 2>>$LOGFILE
    ls -lah $BACKUP_PATH/$BACKUP_FILE".gz" >> $LOGFILE
    echo "BACKUP CORRECTO" >> $LOGFILE
else
    echo "Ha habido un error en el volcado, mirar el mensaje devuelto por mysqldump justo aqui encima." >> $LOGFILE
    echo "BACKUP ERRONEO" >> $LOGFILE
fi
echo "=================================================" >> $LOGFILE

