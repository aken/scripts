#!/bin/bash

state=$(sudo /etc/init.d/fail2ban status | grep active | grep active | cut -d "s" -f 1)

echo $(hostname -A) > ./check_fail2ban/resultado.txt
echo "-------------------------------------------------------------------------------" >> ./check_fail2ban/resultado.txt
echo -e "El estado de fail2ban es $state \n" >> ./check_fail2ban/resultado.txt

#HACEMOS LISTA DE BANEOS Y HORAS
grep -e "Unban" -e "Ban" /var/log/fail2ban.log.1 | cut -d " " -f 2 > ./check_fail2ban/time.txt
grep -e "Unban" -e "Ban" /var/log/fail2ban.log.1 | cut -d "E" -f 2 > ./check_fail2ban/ban.txt
paste ./check_fail2ban/time.txt ./check_fail2ban/ban.txt > ./check_fail2ban/ban_unban.txt

echo "Esta es la lista de IPs baneadas y desbaneadas durante las ultimas 24 horas" >> ./check_fail2ban/resultado.txt
echo "-------------------------------------------------------------------------------" >> ./check_fail2ban/resultado.txt

#abrimos el fichero para sacar los datos y pasarlos a una variable
cat ./check_fail2ban/ban_unban.txt | \
#IFS es el separador de campo
while IFS=" " read time bantype ban ip
do
	nsresult=$(sudo nslookup $ip | grep name | cut -d "=" -f 2)
	echo $time" - "$bantype" - "$ip" - "$ban" - "$nsresult >> ./check_fail2ban/resultado.txt
	echo " " >> ./check_fail2ban/resultado.txt
done

rm ./check_fail2ban/time.txt ./check_fail2ban/ban.txt ./check_fail2ban/ban_unban.txt
exit
