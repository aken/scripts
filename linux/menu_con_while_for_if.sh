#!/bin/bash
# PRUEBA DE MENU UTILIZANDO ARRAYS, CASE, IF y FOR

colores[0]="verde"
colores[1]="rojo"
colores[2]="azul"
colores[3]="blanco"
colores[4]="cyan"

Amarillo="\e[1;33m"
Blanco="\e[1;97m"
Rojo="\e[1;31m"
Verde="\e[1;32m"
Azul="\e[1;34m"
Cyan="\e[1;36m"

while [ true ]
do
	echo -e $Amarillo
	echo "**************************************************"
	echo "**                                              **"
	echo "** Los colores disponibles son:                 **"
	echo "**      blanco,verde,azul,rojo y cyan           **"
	echo "**                                              **"
	echo "**************************************************"
	echo "¿Que color quieres? (fin para acabar)"
	read -r opcion
	case ${opcion} in
		verde)
			echo -e $Verde
			echo "has elegido verde"
			;;
		blanco)
			echo -e $Blanco
			echo "has elegido blanco"
			;;
		cyan)
			echo -e $Cyan
			echo "has elegido cyan"
			;;
		rojo)
			echo -e $Rojo
			echo "has elegido rojo"
			;;
		azul)
			echo -e $Azul
			echo "has elegido azul"
			;;
		fin)
			exit 0
			;;
		*)
			if [[ ${colores[*]} != ${opcion} ]]
			then
				echo -e "\e[1;35m"
				echo "Opcion elegida: "$opcion
				echo "Ese color no lo conozco, los colores que conozco son:"
				for color in ${colores[@]};
				do
					echo "   "$color
				done
			else
				echo -e "\e[1;37m;45m"
				read -p "RAREZA EN EL IF, pulsa una tecla y elige otra opcion "
			fi
			;;
	esac
done

