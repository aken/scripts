#!/bin/bash

#para que se arranque al logarse hay que modificar el fichero /etc/passwd y en vez de definir el shell como /bin/bash (por ejemplo) hay que definirlo como /bin/jaula.sh (por ejmeplo)
#si el usuario pudiese ejecutar ciertos comandos como por ejemplo un reboot habrá que añadirle al fichero sudoers, por ejemplo:  testuser ALL=(ALL) NOPASSWD:ALL
while true
do
INPUT='/tmp/input'
dialog \
    --title "SESION LIMITADA" \
    --backtitle "SHELL SCRIPT CON LIMITACIONES DE EJECUCION DE COMANDOS" \
    --menu "Escoja una opcion" \
    0 100 5 \
    LIST_LOGS "Ver directorio de logs" \
    VER_LOG "Ver un log específico" \
    TOP "Ver procesos del sistema" \
    MOUNTS "Ver montajes" \
    SALIR "Salir" 2>"${INPUT}"
SELECCIONADO=$(<"${INPUT}")

case ${SELECCIONADO} in
    LIST_LOGS)
        clear
        ls -la /var/log | more;;
    VER_LOG)
        dialog --title "VER LOG" --inputbox "Nombre del log (sin path):" 2>${INPUT} 0 40
        clear
        LOG="/var/log/"$(cat $INPUT)
        dialog --title "LOG A VER" --textbox "LOG: $LOG" 0 0
        less $LOG;;
    TOP)
        top -d 1;;
    MOUNTS)
        df -h > /tmp/jaula.out
        #sudo fdisk --list >> /tmp/jaula.out
        less /tmp/jaula.out
        rm /tmp/jaula.out;;
    SALIR)
        clear
        exit;;
esac

rm ${INPUT}
done
