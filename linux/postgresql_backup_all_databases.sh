#!/bin/bash


BACKUP_PATH="/var/data/postgresql_backup/"
BACKUP_FECHA=$(date +%d-%m-%Y_%H-%M)
DIASMANTENER="30"
SERVIDOR=$(hostname -f)

# Nos movemos a este directorio para que el pg_dump no de error de no acceso a /root/scripts (si usamos este directorio para ejecutar el script)
cd $BACKUP_PATH

#Creamos lista de bases de datos para realizar backup
LISTA_DATABASES=$(sudo -u postgres /usr/bin/psql -l -t | cut -d'|' -f1 | sed -e 's/ //g' -e '/^$/d')

#Comenzamos tarea de backup
for DATABASE in $LISTA_DATABASES; do
	if [ "$DATABASE" != "template0" ] && [ "$DATABASE" != "template1" ]; then
		LOGFILE=$BACKUP_PATH$DATABASE"_backup.log"
		BACKUP_FILE=$BACKUP_PATH$DATABASE"_"$BACKUP_FECHA".sql"
		true > $LOGFILE
		echo "=================================================" >> $LOGFILE
		echo $SERVIDOR >> $LOGFILE
		echo "BACKUP BBDD $DATABASE - Realizado el $now" >> $LOGFILE
		sudo -u postgres /usr/bin/pg_dump -Fc $DATABASE > $BACKUP_FILE 2>>$LOGFILE
		if [ "$?" -eq 0 ]
		then
			echo "Volcado de la base de datos $DATABASE correcto. Comprimiendo con gzip." >> $LOGFILE
			chown postgres:postgres $BACKUP_FILE
			chmod 600 $BACKUP_FILE
			gzip -9 $BACKUP_FILE 2>>$LOGFILE
			echo "----------------------" >> $LOGFILE
			echo "BACKUP CORRECTO" >> $LOGFILE
		else
			echo "Ha habido un error en el volcado, mirar el mensaje devuelto por postgres justo aqui encima." >> $LOGFILE
			echo "----------------------" >> $LOGFILE
			echo "BACKUP ERRONEO" >> $LOGFILE
		fi
	fi
done

#Realizamos tareas de limpieza de todos los ficheros existentes en el directorio de backup
find $BACKUP_PATH -type f -prune -mtime +$DIASMANTENER -exec rm -f {} \;
