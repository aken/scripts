#!/bin/bash
LAST_KERNEL=$(rpm -q --last kernel | perl -pe 's/^kernel-(\S+).*/$1/' | head -1)
CURRENT_KERNEL=$(uname -r)

if [[ $LAST_KERNEL != $CURRENT_KERNEL ]]
then
        echo "REQUIERE REINICIO KERNEL" > ./check_reboot_needed.txt
else
        NEEDRESTART=$(needs-restarting -r | grep 'Reboot is required' | cut -b 1-18)
        if [[ $NEEDRESTART == "Reboot is required" ]]
        then
                echo "REQUIERE REINICIO OTHER" > ./check_reboot_needed.txt
        else
                echo "NO NECESITA REINICIARSE" > ./check_reboot_needed.txt
        fi
fi
exit
