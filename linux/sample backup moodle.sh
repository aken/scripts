#!/bin/bash
# -*- ENCODING: UTF-8 -*-
echo '------------------'
echo '-Iniciando backup-'
echo '------------------'
date '+%A %T'

#Declaramos varibles a usar
dir_base="/var/www/vhosts/web.es/httpdocs/web/web"
dir_storage="/var/www/vhosts/web.es/httpdocs/backup-pruebas/web"
dir_storage_nfs="/var/www/vhosts/web.es/httpdocs/backup-storage/web/"

cd $dir_base
fecha=$(date '+%F')
usu_bd='usuario'
pass_bd='password'
name_bd='moodle'

#Activamos modo mantenimiento forzando la ejecución con PHP 5.6 (moodle 3.3 lo requiere)
/opt/plesk/php/5.6/bin/php www/admin/cli/maintenance.php --enable

#Eliminar backups antiguos: Anteriores a 15 días
find $dir_storage/*.zip -mtime +14 -type f -delete
find $dir_storage/*.sql -mtime +14 -type f -delete
find $dir_storage/*.gz -mtime +14 -type f -delete
echo '--- Backups previos eliminados ---'

dir='www'
echo '--- ZIP '$dir' ---'
printf "\n" >> $dir_storage/backup.log
echo '--- ZIP '$dir' ---' >> $dir_storage/backup.log
fichero=$fecha-$dir.zip
f_ini=$(date '+%T')
zip -rq $dir_storage/$fichero $dir
f_fin=$(date '+%T')
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero
printf "\n" >> $dir_storage/backup.log
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log
f_ini=$(date '+%T')
mv $dir_storage/$fichero $dir_storage_nfs
f_fin=$(date '+%T')
printf "\n" >> $dir_storage/backup.log
echo '   Fichero movido ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log

dir='moodledata'
echo '--- ZIP '$dir' ---'
printf "\n" >> $dir_storage/backup.log
echo '--- ZIP '$dir' ---' >> $dir_storage/backup.log
fichero=$fecha-$dir.zip
f_ini=$(date '+%T')
zip -rq $dir_storage/$fichero $dir
f_fin=$(date '+%T')
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero
printf "\n" >> $dir_storage/backup.log
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log
f_ini=$(date '+%T')
mv $dir_storage/$fichero $dir_storage_nfs
f_fin=$(date '+%T')
printf "\n" >> $dir_storage/backup.log
echo '   Fichero movido ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log

echo '--- DUMP GZIP BBDD ---'
printf "\n" >> $dir_storage/backup.log
echo '--- DUMP GZIP BBDD ---' >> $dir_storage/backup.log
fichero=$fecha-bbdd.sql.gz
f_ini=$(date '+%T')
mysqldump --user=$usu_bd --password=$pass_bd $name_bd | gzip > $dir_storage/$fichero
f_fin=$(date '+%T')
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero
printf "\n" >> $dir_storage/backup.log
echo '   Proceso completo ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log
f_ini=$(date '+%T')
mv $dir_storage/$fichero $dir_storage_nfs
f_fin=$(date '+%T')
printf "\n" >> $dir_storage/backup.log
echo '   Fichero movido ['$f_ini']-['$f_fin']: '$fichero >> $dir_storage/backup.log

#Desactivamos modo mantenimiento forzando la ejecución con PHP 5.6 (moodle 3.3 lo requiere)
/opt/plesk/php/5.6/bin/php www/admin/cli/maintenance.php --disable

echo '-------------------'
echo '-Backup finalizado-'
echo '-------------------'
date '+%A %T'
