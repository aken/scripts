#!/bin/bash

if (($# != 1));
then
	echo "0"
else
	# Obtain day, month and year of certificate expiration
	expiration_date=`echo | openssl s_client -servername $1 -connect $1:443 2>/dev/null | openssl x509 -noout -enddate | cut -d= -f2- | awk '{print $2"-"$1"-"$4}'`

	# Get certificate expiration date in epoch format
	cert_epoch=`date --date="$expiration_date" +%s`

	# Get current date in epoch format
	curr_epoch=`date +%s`

	# Get the difference and convert to days
	difference=`expr $(( (cert_epoch - $curr_epoch) / 86400))`
	
	if [ "$difference" -le 0 ]; then
			# Print 0 in case of invalid certificate
			echo "0"
	else
			# Days untill expiration
			echo "$difference"
	fi
fi