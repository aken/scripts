#!/bin/bash

nombres=(
    'nombre1'
    'nombre2'
    '...')
sufijo=".dominioquesea.es"

true > jaulas.local.inc

#creamos un fichero con todas las jaulas para luego incluirlo en el jail.local de fail2ban
for nombre in "${nombres[@]}"
do
    echo "Creando la jaula $nombre"
    {
    echo "[$nombre]"
    echo "enabled = true"
    echo "filter = $nombre$sufijo"
    echo "port = http,https"
    echo "logpath = /var/log/nginx/$nombre""_access.log"
    echo "banaction = firewallcmd-ipset"
    echo "maxretry = 3000"
    echo "bantime =  3600"
    echo "findtime = 3600"
    echo ""
    echo "[$nombre-loop]"
    echo "enabled = true"
    echo "filter = $nombre$sufijo"
    echo "port = http,https"
    echo "logpath = /var/log/nginx/$nombre""_access.log"
    echo "banaction = firewallcmd-ipset"
    echo "maxretry = 5999"
    echo "bantime =  86400"
    echo "findtime = 7200"
    echo ""
    } >> jaulas.local.inc
done

#creamos todos los ficheros necesarios para los filtros que luego hay que mover a /etc/fail2ban/filter.d
for nombre in "${nombres[@]}"
do
    echo "Creando el filtro $nombre$sufijo.conf"
    {
    echo "[Definition]"
    echo 'failregex = ^<HOST> -.*"(GET)*("http://'$nombre$sufijo'/")*$'
    } > $nombre$sufijo.conf
done
