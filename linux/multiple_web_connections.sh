#!/bin/bash

if [ $# -ne 2 ]
then
    echo "===="; echo "Uso: $0 [web] [conexiones]"; echo "====";
    exit 0
fi
if [ $2 -gt 0 ]
then
    times=$2
else
    times=1
fi
echo "Se van a realizar $times conexiones a la web $1"
read -p "Pulse [ENTER] para comenzar."
while [ $times -gt 0 ]
do
    echo "Conexiones restantes $times"
    wget $1 --delete-after &
    times=$(($times-1))
    rm wget-log*
done
rm wget-log*
echo "Done"
