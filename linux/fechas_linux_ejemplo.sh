
#!/bin/bash

HOY=$(date +%Y-%m-%d)
FECHAFIN="$HOY 06:26:00"
HORAACTUAL=$(date +"%T")
FECHAACTUAL="$HOY $HORAACTUAL"

echo "Hora actual: " $HORAACTUAL
echo "Hora fin: " $HORAFIN
echo "Fecha actual: " $FECHAACTUAL
echo "Fecha fin: " $FECHAFIN

#en segundos
DIF_SEGUNDOS=$(( ($(date --date "$FECHAFIN" +%s) - $(date --date "$FECHAACTUAL" +%s) ) ))
#en minutos
DIF_MINUTOS=$(( ($(date --date "$FECHAFIN" +%s) - $(date --date "$FECHAACTUAL" +%s) )/(60) ))
#en horas
DIF_HORAS=$(( ($(date --date "$FECHAFIN" +%s) - $(date --date "$FECHAACTUAL" +%s) )/(60*60) ))
#en dias
DIF_DIAS=$(( ($(date --date "$FECHAFIN" +%s) - $(date --date "$FECHAACTUAL" +%s) )/(60*60*24) ))

echo "Me tengo que esperar " $DIF_SEGUNDOS " segundos"

if [ $DIF_SEGUNDOS -gt 2 ]
then
  sleep $DIF_SEGUNDOS
fi

echo "Ya son las " $(date +"%T")
