#!/bin/bash
FPM_MASTER_PID=4422

while true;
do
  printf -- '-%.0s' {1..100}; echo ""
  date --iso-8601=seconds
  echo ""
  pstree $FPM_MASTER_PID -c -l -p  -U
  echo ""
  ps -ylC php-fpm --sort:rss -u nginx
  echo ""
  ps --no-headers -o "rss,cmd" -C php-fpm -u nginx | awk '{ sum+=$1 } END { printf ("%d%s\n", sum/NR/1024,"M") }'
  echo ""
  ps -eo size,pid,user,command --sort -size | awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' | grep "php-fpm:"
  echo ""
  sleep 2
done
