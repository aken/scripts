#!/bin/bash

#Tarea de Backup Bases de datos POSTGRESQL de GITLAB-CE
backup_dir="/var/data/backup/"

#Añadimos la fecha de backup al nuevo archivo
backup_fecha=$(date +%d-%m-%Y_%H:%M)
echo "Fecha de backup: $backup_fecha"

#Número de días que vamos a mantener nuestros backups
numero_dias="30"

#Creamos lista de bases de datos para realizar backup
bases_de_datos=$(sudo su - gitlab-psql -c "/opt/gitlab/embedded/bin/psql -l -t -h /var/opt/gitlab/postgresql -p 5432 | cut -d'|' -f1 | sed -e 's/ //g' -e '/^$/d'")
echo "Bases de datos encontradas:"
for i in $bases_de_datos; do
  if [ "$i" != "template0" ] && [ "$i" != "template1" ]; then
    echo "$i"
  fi
done

#Comenzamos tarea de backup
#El nombre del fichero de backup dera:  nombre de la base de datos + fecha + estensión .gz
for i in $bases_de_datos; do
  if [ "$i" != "template0" ] && [ "$i" != "template1" ]; then
    volcado="$backup_dir$i""_$backup_fecha"".psql.gz"
    echo "Volcamos $i a $volcado"
    sudo su - gitlab-psql -c "/opt/gitlab/embedded/bin/pg_dump -Fc $i -h /var/opt/gitlab/postgresql -p 5432 | gzip > $volcado"
  fi
done

echo "Backups finalizados, limpiamos los backups más antiguos de $numero_dias días"
#Realizamos tareas de limpieza
sudo find $backup_dir -type f -prune -mtime +$numero_dias -exec rm -f {} \;
