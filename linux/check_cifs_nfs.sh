#!/bin/bash
if [ "$#" -eq "0" ]
then
  echo "Debe pasar la IP a comprobar: $0 ip"
  exit
fi
IP=$1
echo "Comprobando hostname de $IP"
HOSTNAME=$(ssh "$IP" "hostname -f")
echo -e "Hostname: $HOSTNAME \nComprobando sistemas cifs / nfs"
cifs_fs=$(ssh globaluser@"$IP" "sudo df -t cifs -h")
nfs_fs=$(ssh globaluser@"$IP" "sudo df -t nfs -h")
nfs4_fs=$(ssh globaluser@"$IP" "sudo df -t nfs4 -h")
echo -e "CIFS:\n$cifs_fs\n"
echo -e "NFS:\n$nfs_fs\n"
echo -e "NFS4:\n$nfs4_fs\n"
cifs=$(echo "$cifs_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
nfs=$(echo "$nfs_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
nfs4=$(echo "$nfs4_fs" | tail -n +2 | awk -F " " '{ print $1"#"$2"#"$3"#"$4"#"$5"#"$6 }')
echo "hostname#ip#tipo#export#tamaño#usado#disponible#%uso#montado en"
if [[ "$cifs" != "" ]]; then
   for fs in $cifs; do
       echo "$HOSTNAME $IP cif $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }'
   done
fi
if [[ "$nfs" != "" ]]; then
   #nfsVersion=$(ssh globaluser@"$IP" "sudo nfsstat -c | grep 'Client nfs' | awk -F ":" '{ print \$1 }' | awk '{ print \$3 }' | paste -s -d_") # el paste es por si hay varias versiones en varias lineas las concatene
   for fs in $nfs; do
       echo "$HOSTNAME $IP nfs_v3 $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }'
   done
fi
if [[ "$nfs4" != "" ]]; then
    for fs in $nfs4
    do
        echo "$HOSTNAME $IP nfs_v4 $fs" | awk '{ print $1"#"$2"#"$3"#"$4 }'
    done
fi
