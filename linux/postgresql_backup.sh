#!/bin/bash


DATABASE="database"
BACKUP_PATH="/var/data/postgres_backup"
BACKUP_FILE=$DATABASE"-"$(date +%Y-%m-%d-%H-%M)".sql"
LOGFILE=$BACKUP_PATH"/"$DATABASE"_backup.log"
SERVIDOR=$(hostname -f)
DIASMANTENER=15

now=$(date +"%d-%m-%Y")

true > $LOGFILE

# Nos movemos a este directorio para que el pg_dump no de error de no acceso a /root/scripts (si usamos este directorio para ejecutar el script)
cd $BACKUP_PATH

echo "=================================================" >> $LOGFILE
echo "Servidor: $SERVIDOR" >> $LOGFILE
echo "BACKUP BBDD $DATABASE - Realizado el $now" >> $LOGFILE
echo "BORRAMOS LOS BACKUPS QUE TIENEN MAS DE $DIASMANTENER DIAS DE ANTIGUEDAD" >> $LOGFILE
find  $BACKUP_PATH/ -type f -name $DATABASE"*" -mtime +$DIASMANTENER -delete
echo "CREANDO COPIA DE SEGURIDAD CON postgres" >> $LOGFILE
sudo -u postgres /usr/bin/pg_dump $DATABASE > $BACKUP_PATH/$BACKUP_FILE  2>>$LOGFILE
if [ "$?" -eq 0 ]
then
    echo "Volcado de la base de datos $DATABASE correcto. Comprimiendo con gzip." >> $LOGFILE
    gzip -9 $BACKUP_PATH/$BACKUP_FILE 2>>$LOGFILE
	chown postgres:postgres $BACKUP_PATH/$BACKUP_FILE".gz"
	chmod 600 $BACKUP_PATH/$BACKUP_FILE".gz"
    ls -lah $BACKUP_PATH/$BACKUP_FILE".gz" >> $LOGFILE
    echo "BACKUP CORRECTO" >> $LOGFILE
else
    echo "Ha habido un error en el volcado, mirar el mensaje devuelto por postgres justo aqui encima." >> $LOGFILE
    echo "BACKUP ERRONEO" >> $LOGFILE
fi
echo "=================================================" >> $LOGFILE


