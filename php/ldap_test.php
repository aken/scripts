<?php
// ejemplo de autenticación
$ldaprdn  = 'USUARIO';     // ldap rdn or dn
$ldappass = 'PASSWORD';  // associated password
// conexión al servidor LDAP
$ldapconn = ldap_connect("DOMINIO",389)
    or die("Could not connect to LDAP server.");
if ($ldapconn) {
    // realizando la autenticación
    $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
    // verificación del enlace
    if ($ldapbind) {
        echo "LDAP bind successful...";
    } else {
        echo "LDAP bind failed...".$ldapbind;
    }
}
?>
